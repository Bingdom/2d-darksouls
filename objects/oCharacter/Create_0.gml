/// @description Initialisation

getTimingPreferences();
getTimingStats();
getStats();


charState = groundWalk;

collisionMap = layer_tilemap_get_id(layer_get_id("Tiles"))

x1 = 0;
y1 = 0;
x2 = 0;
y2 = 0;

xSpeed = 0;
ySpeed = 0;

y=room_height*.5;

hitColliderInst = noone; //During an attack. This carries the id of the hitbox