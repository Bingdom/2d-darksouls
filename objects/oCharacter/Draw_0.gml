/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_text_colour(x, y, script_get_name(charState)+"\n"+string(charShieldIsAction), c_black, c_black, c_black, c_black, 1);

draw_text_colour(x, y-8, point_distance(x1, y1, x2, y2), c_black, c_black, c_black, c_black, 1);

if charState == groundAttack {
	switch(charAttackStage) {
		case ATK_STAGE_WINDUP:
			if charAttackType == ATK_SWING_LIGHT {
				var atkProgress = (get_timer_ms()-charAttackTimer)/charAttackAnticiLT; //0-1
			} else {
				var atkProgress = (get_timer_ms()-charAttackTimer)/charAttackAnticiHY; //0-1
			}
			draw_rectangle_colour(x-(1-abs(atkProgress-0.5)*2)*30,y-2,x,y+2,c_orange,c_orange,c_orange,c_orange,false);
			draw_line_colour(x,y-6,x,y+6,c_orange,c_orange);
		break;
		
		case ATK_STAGE_RELEASE:
			if charAttackType == ATK_SWING_LIGHT {
				draw_rectangle_colour(x,y-2,x+((get_timer_ms()-charAttackTimer)/charAttackReleaseLT)*30,y+2,c_red,c_red,c_red,c_red,false);
			} else {
				draw_rectangle_colour(x,y-2,x+((get_timer_ms()-charAttackTimer)/charAttackReleaseHY)*30,y+2,c_red,c_red,c_red,c_red,false);
			}
			draw_line_colour(x,y-6,x,y+6,c_red,c_red);
		break;
		
		case ATK_STAGE_RECOVER:
			draw_rectangle_colour(x,y-2,x+(1-(get_timer_ms()-charAttackTimer)/charAttackRecover)*30,y+2,c_yellow,c_yellow,c_yellow,c_yellow,false);
			draw_line_colour(x,y-6,x,y+6,c_yellow,c_yellow);
		break;
	}
	
}

//Draw circle so we know the player is blocking
if charIsBlocking {
	var col = merge_colour(c_green, c_red, (get_timer_ms()-charShieldTimer)/charShieldMax);
	draw_circle_colour(x, y, 16, col, col, true);
}