/// @description Insert description here
// You can write your code in this editor

draw_healthbar(1,1,151,19, (charHP/charHPMax) * 100,c_yellow,c_red,c_red,0,true,true);
draw_text_colour(1,2,charHP,c_black,c_black,c_black,c_black,1);
draw_healthbar(1,23,151,41, (charStamina/charStaminaMax) * 100,c_yellow,c_yellow,c_yellow,0,true,true);
draw_text_colour(1,24,charStamina,c_black,c_black,c_black,c_black,1);