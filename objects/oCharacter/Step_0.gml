/// @description State machine

//Ensure that the oController's step event occurs before this one. You can check by checking the instance creation
//order in room properties (oController should be first).
//oController needs to be first so we get this frame.
delta = oController.delta;

ySpeed+= charGrav*delta;

/*
charStaminaRegenStart =2500; //If the player doesn't perform stamina based actions after this time, they'll start regenerating stamina
charStaminaRegenTimer =  -1; //Time since the player did their last move
charStamReg    = .05; //The rate it will regenerate per ms
*/

//Handle stamina
if charStaminaRegenTimer == -1 {
	charStamina = min(charStaminaMax, charStamina + (charStamReg * delta * 1000) / GAMESTEP );
} else {
	if charStaminaRegenStart <= get_timer_ms() - charStaminaRegenTimer {
		//Find how much we were off this frame
		//Then apply it to charStamina
		charStamina = min(charStaminaMax, charStamina + (get_timer_ms() - charStaminaRegenTimer - charStaminaRegenStart) * charStamReg);
		
		charStaminaRegenTimer = -1;
	}
}


//We'll use a state machine to handle all the different states the player may be in.
//We will not use script_execute (although we can) because that function checks every script on the resource tree.
//Therefore, doing it manually is slightly faster

switch(charState) {
	case groundWalk:
		groundWalk();
	break;
	
	case groundAttack:
		groundAttack();
	break;
	
	case groundShieldBlock:
		groundShieldBlock();
	break;
	
	case airJump:
		airJump();
	break;
	
	case groundKickedback:
		groundKickedback();
	break;
}

x += xSpeed*delta;
y += (1/2)*(charGrav*abs(sign(ySpeed)))*sqr(delta) + ySpeed*delta;