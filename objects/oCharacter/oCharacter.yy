{
    "id": "a7afb641-7244-484c-bc1d-7d89cbb4b378",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCharacter",
    "eventList": [
        {
            "id": "b867dd83-eafc-45ab-95ba-c8d202f2c15f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7afb641-7244-484c-bc1d-7d89cbb4b378"
        },
        {
            "id": "cb2e2e7b-a1bd-4cee-a6a0-2442f89d9685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a7afb641-7244-484c-bc1d-7d89cbb4b378"
        },
        {
            "id": "00841afd-e770-426d-8b8a-3d86de3febee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a7afb641-7244-484c-bc1d-7d89cbb4b378"
        },
        {
            "id": "099333f8-820b-4f86-8f82-69e7df2697bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a7afb641-7244-484c-bc1d-7d89cbb4b378"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}