/// @description 
#macro GAMESTEP 60 //This is the target update rate. 
//We use GAMESTEP to set a hard limit. This prevents faster refresh rates (fps) from running the game faster

//show_debug_overlay(true)

game_set_speed(60, gamespeed_fps);