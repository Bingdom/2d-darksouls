{
    "id": "6107e18a-8815-411b-9eb2-8efad2f85a24",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oController",
    "eventList": [
        {
            "id": "856ab72f-1f43-4546-82cf-50eed9137394",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6107e18a-8815-411b-9eb2-8efad2f85a24"
        },
        {
            "id": "da77fe47-23a6-4c37-b399-3958e78e651b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6107e18a-8815-411b-9eb2-8efad2f85a24"
        },
        {
            "id": "4500fb00-3f54-4603-93ee-a0c55a0c2e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6107e18a-8815-411b-9eb2-8efad2f85a24"
        },
        {
            "id": "e8a49f58-dfdb-4d5d-9708-52bd8b2f22b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 116,
            "eventtype": 5,
            "m_owner": "6107e18a-8815-411b-9eb2-8efad2f85a24"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}