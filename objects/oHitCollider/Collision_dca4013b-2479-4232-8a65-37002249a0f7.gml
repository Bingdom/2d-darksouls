/// @description Attack
if other.id != host.id {
	//We will not do a team check for now.
	
	//Prevent hitting the same enemy twice
	hitBefore = false;
	for(var i=0; i<ds_list_size(hitList); i++) {
		//									Make sure they are not attacking too
		if hitList [| i] == other.id and other.charAttackStage != ATK_STAGE_RELEASE {
			hitBefore = true;
			break;
		}
	}
	
	//We have not hit them before. Hit now
	if !hitBefore {
		ds_list_add(hitList, other.id);
		
		hitInstance(other.id, host.charHitRecieveTime, host.charHitKickbackSpd * sign(host.image_xscale));
	}
}