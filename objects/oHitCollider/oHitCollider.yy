{
    "id": "2f6dfcba-4dc8-4610-9303-4c883b9808fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHitCollider",
    "eventList": [
        {
            "id": "dca4013b-2479-4232-8a65-37002249a0f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7afb641-7244-484c-bc1d-7d89cbb4b378",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f6dfcba-4dc8-4610-9303-4c883b9808fe"
        },
        {
            "id": "98b69ec6-fbf2-4588-8ebf-89e9f13dcf19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f6dfcba-4dc8-4610-9303-4c883b9808fe"
        },
        {
            "id": "08ea52b6-327a-49f7-a5d6-c70003d1d99a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2f6dfcba-4dc8-4610-9303-4c883b9808fe"
        },
        {
            "id": "e8afa466-5766-4073-a318-81b0e19bb4b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "2f6dfcba-4dc8-4610-9303-4c883b9808fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28a6cddf-e11c-47a9-a2e7-208094481127",
    "visible": true
}