//This just assigns variables
//For getting active input, use getInput
//key is used for getting input
//move is the action of moving. This is also used for ai
switch(argument0) {
	case CONTROL_CONTROLLER:
		//Gamepad controls
		
		//------ Combat --------
		keyAtkLT  = gp_shoulderr;
		keyAtkHY  = gp_shoulderrb;
		keyCancel = gp_face2;
		keyBlock  = gp_shoulderlb;
	
		//----- keyment -------
		keyRight  = gp_padr;
		keyUp	  = gp_face1;
		keyLeft   = gp_padl;
		keyDown   = gp_padd;
	
		keyRun    = gp_face3; //key press to run
		
		keyStart = 0; //Used to keep track on the previous horizontal axis state. To check if it's recently been pressed or not (for dash).
	break;
	
	case CONTROL_KEYBOARD:
		//keyboard controls
	
		//------ Combat --------
		keyAtkLT  = ord("J");
		keyAtkHY  = ord("K");
		keyCancel = ord("O");
		keyBlock  = ord("L");
	
		//----- keyment -------
		keyRight  = ord("D");
		keyUp	  = ord("W");
		keyLeft   = ord("A");
		keyDown   = ord("S");
	
		keyRun    = ord("M"); //key press to run
	break;
}