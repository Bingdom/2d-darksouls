///getPlayerInput(CONTROL_KEYBOARD|CONTROL_CONTROLLER)
///@param CONTROL_KEYBOARD|CONTROL_CONTROLLER
/* 
 * A script that sets a bunch of variables to control an instance
 */

#macro CONTROL_KEYBOARD 0
#macro CONTROL_CONTROLLER 1

switch(argument0) {
	case CONTROL_KEYBOARD:
		//------ Combat --------
		moveAtkLT  = keyboard_check(keyAtkLT);
		moveAtkHY  = keyboard_check(keyAtkHY);
		moveCancel = keyboard_check(keyCancel);
		moveBlock  = keyboard_check(keyBlock);
	
		//----- movement -------
		moveRight  = keyboard_check(keyRight);
		moveUp	   = keyboard_check(keyUp);
		moveLeft   = keyboard_check(keyLeft);
		moveDown   = keyboard_check(keyDown);
		
		moveLeftPressed  = keyboard_check_pressed(keyLeft);
		moveRightPressed = keyboard_check_pressed(keyRight);
	
		moveRun    = keyboard_check(keyRun);
	break;
	
	case CONTROL_CONTROLLER:
		gamepad_set_axis_deadzone(0, 0.05);
	
		moveAtkLT  = gamepad_button_check(0, keyAtkLT);
		moveAtkHY  = gamepad_button_check(0, keyAtkHY);
		moveCancel = gamepad_button_check(0, keyCancel);
		moveBlock  = gamepad_button_check(0, keyBlock);
	
		//----- movement -------
		//Use d-pad
		if gamepad_button_check(0, keyRight) || gamepad_button_check(0, keyLeft) {
			moveRight = gamepad_button_check(0, keyRight);
			moveLeft = gamepad_button_check(0, keyLeft);
			
			moveDown = gamepad_button_check(0, keyDown);
			moveUp = gamepad_button_check(0, keyUp);
			
			moveLeftPressed = gamepad_button_check_pressed(0, keyLeft);
			moveRightPressed = gamepad_button_check_pressed(0, keyRight);
		} else {
			//Use analog stick
			moveRight = gamepad_axis_value(0, gp_axislh) > 0 ? 1 : 0;
			moveLeft = gamepad_axis_value(0, gp_axislh) < 0 ? 1 : 0;
			
			moveDown = gamepad_axis_value(0, gp_axislv) < -0.3 ? 1 : 0;
			moveUp = gamepad_axis_value(0, gp_axislv) > 0.3 ? 1 : 0;
			
			moveLeftPressed = false;
			moveRightPressed = false;
			
			if !moveLeft || !moveRight {
				keyStart = true;
			}
			
			if keyStart {
				if moveLeft {
					keyStart = false;
					moveLeftPressed=true;
				} else if moveRight {
					keyStart = false;
					moveRightPressed=true;
				}
			}
		}
		
		moveRun = gamepad_button_check(0, keyRun);
		
		/*moveRight  = 0;
		moveUp	   = 0;
		moveLeft   = 0;
		moveDown   = 0;
		
		moveLeftPressed  = 0;
		moveRightPressed = 0;
	
		moveRun    = 0;*/
	break;
}