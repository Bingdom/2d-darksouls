//------ Health --------
charHPMax			= 100;
charHP				= charHPMax;

//----- Stamina --------
charStaminaMax		= 100;
charStamina			= charStaminaMax;
charStamReg			= .05; //The rate it will regenerate per ms

//- Staimina Costs --
charStamRun			=   0; //It will not cost anything to run
charStamAtkLT		=  23; //Light attack
charStamAtkHY		=  59; //Heavy attack
charStamDash		=  17; //Dash


//------ Attack --------

//----- Kickback -----
charHitKickbackSpd	=  3; //How fast they should be kickedback by

//----- Movement -------

//--- Max - Ground ---
charWalkSpeed		= 3;
charRunSpeed		= 7;
charDashSpeed		= 23;


//---- Max - Air -----
charJumpStr			= 9;

//---- Acc - Air -----
charGrav			= 0.7;

//---- Shield User -----
charHasShield		= true;
charIsBlocking		= false;