/**
 * The difference this makes compared to getTimingsStats is this one is player customisable. The other one is not, and based off
 * the game's mechanics. The reason it's separate is to make it easier to setup multiple characters.
 *
 * In this project, variables starting with a "thres" (short for threshold) would be bound to player controls
 */

//----- Movement -------
thresIsRunning    = false; //Are we running?

//------ Dash --------
thresDash           = 300; //(In ms) How long the same move has to pressed in order to perform a dodge move.
thresDashTime	    =   0; //Time since the last move was pressed
thresDashmove        =  -1; //Keep track of the last move pressed.

//------ Attack --------
thresAttackStrong   = 200; //We'll use a separate move for now ---- How long it would take to turn a weak attack into a strong one (From holding a move)
thresAttackTime     =   0; //Just to keep track since the move was pressed


//-------- Jump --------
thresJumpAgain      = 500; //How long the player can jump again. It's just to prevent the player from rapidly jumping if they're in a tight corridor (for example).
thresJumpAgainTime  =   0; //Holds last time the player jumped