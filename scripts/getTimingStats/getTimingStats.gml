/**
 * The difference this makes compared to getTimingsPreference is this one is NOT player customisable. The other one is.
 * The reason it's separate is to make it easier to setup multiple characters.
 *
 * In this project, variables starting with a "char" (short for character) would be character specific.
 */
//Attack type
#macro ATK_SWING_LIGHT 0
#macro ATK_SWING_HEAVY 1

//Attack Stages
#macro ATK_STAGE_WINDUP   0
#macro ATK_STAGE_RELEASE  1
#macro ATK_STAGE_RECOVER  2

//----- Stamina --------
charStaminaRegenStart = 900; //If the player doesn't perform stamina based actions after this time, they'll start regenerating stamina
charStaminaRegenTimer =  -1; //Time since the player did their last move

//------ Attack --------
charAttackAnticiLT    = 350;
charAttackAnticiHY    = 600;

charAttackReleaseLT   = 100;
charAttackReleaseHY   = 150;

charAttackRecover     = 200;

charAttackType        =   0; //The type of attack so we get the right timer (#macro light or heavy)
charAttackStage       =   0;
charAttackTimer       =   0;

//----- Kickback -----
charHitRecieveTime    = 200; //How long they should kickbacked by (in ms)
charHitRecieveTimer   =   0;


//----- Movement -------
charDashDuration	  = 800; //How long the player would be in the dash state
charDashTimer		  =   0;

//------ Dash --------
charDashingDur        =  30; //How long the character would dash for
charDashDir           =   0; //Dash direction
charDashingTime       =   0; //Current timer. If it is at 0, we are not dashing

//------- Shield -------
charShieldTimer       =   0;
charShieldRecoTimer   =   0; //Recovery timer
charShieldEquipTimer  =   0; //Equip timer (How long it takes to holster your shield)
charShieldIsAction    =false; //Whether it is holster'ing' or not.
charShieldToUnblock   =false; //Force the toggle action phase to unblock. This is useful if the player decides to unblock during the blocking action
charShieldHasTimer    =true; //The timer does count down. They can't hold their shield for long
charShieldEquip       = 300; //How long it takes to hold the shield in front of the character.
charShieldMax         =1500; //How long the character can block for
charShieldBetween     =1000; //How long the character has to wait between blocks