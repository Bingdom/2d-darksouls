/**
 * In this state, the character cannot move.
 * The player can choose to feint (cancel) their attack
 *
 */

switch(charAttackStage) {
	case ATK_STAGE_WINDUP:
		if charAttackType == ATK_SWING_LIGHT {
			var requiredTime = charAttackAnticiLT;
			var requiredStam = charStamAtkLT;
		} else {
			var requiredTime = charAttackAnticiHY;
			var requiredStam = charStamAtkHY;
		}
		
		if get_timer_ms() - charAttackTimer >= requiredTime {
			//Create object
			hitColliderInst = instance_create_layer(x+sign(image_xscale)*(20+12), y, "Instances", oHitCollider)
			hitColliderInst.host = id;
			
			charAttackTimer = get_timer_ms() + ((get_timer_ms() - charAttackTimer) - requiredTime); //Apply the difference for the next stage
			charAttackStage = ATK_STAGE_RELEASE;
		} else {
			
			//Feint if the player wishes to
			
			
			if (moveCancel) and charStamina >= requiredStam  {
				charState = groundWalk;
				charStaminaTake(requiredStam);
			}
		}
	break;
	
	case ATK_STAGE_RELEASE:
		//At this stage, we deal damage
		
		if charAttackType == ATK_SWING_LIGHT {
			var requiredTime = charAttackReleaseLT;
			var requiredStam = charStamAtkLT;
		} else {
			var requiredTime = charAttackReleaseHY;
			var requiredStam = charStamAtkHY;
		}
		
		if get_timer_ms() - charAttackTimer >= requiredTime {
			charAttackTimer = get_timer_ms() + ((get_timer_ms() - charAttackTimer) - requiredTime); //Apply the difference for the next stage
			charAttackStage = ATK_STAGE_RECOVER;
			
			//If the player has missed, penalise with stamina
			charStaminaTake(requiredStam);
			
			instance_destroy(hitColliderInst);
		}
	break;
	
	case ATK_STAGE_RECOVER:
		if get_timer_ms() - charAttackTimer >= charAttackRecover {
			charState = groundWalk;
		}
	break;
}
 
horizontalCollision();
verticalCollision();