if ((charHitRecieveTimer - ((1/GAMESTEP)*1000)*delta) > 0) {
	//We have plenty of time. kickback normally.
	//xSpeed = charHitKickbackSpd;
	
} else {
	/**
	 * This is for the last kickback frame
	 * This makes sure we are exactly in the right spot after the kickback (The time is not bound to frames, unlike controls)
	 * This is important for not only just precise fighting games, but for multiplayer too. Clients don't have exactly the same "step cycle".
	 *
	 * The underlying principle is this;
	 *   distance = velcity * time
	 *
	 * We have assigned our speed and time. We just make sure the frames don't break that
	 */
	//if (charDashingTime > 0) {
		//Note: This is probably bugged. The player jumps back a bit.
	
		//Find the difference from the last frame, since we are delta timing and want unlocked fps (rather than using fixed numbers).
		//We add charDashingDur because it's supposed to have that difference (look at the first if condition)
		var thresKickRemaining = charHitRecieveTimer;
		//thresDashRemaining is the remaining ms. It would always be less than 1/TARGET_FPS.
		
		show_debug_message("Time remaining: " + string(thresKickRemaining));
		
		//Since the movement variable is not dependant on the instance, but depenendant on an outside instance,
		//we'll take the speed from xSpeed
		var charTempHitKickbackSpd = xSpeed;
		
		//Find our speed in pixels per ms
		//Speed per frame * Speed per second / ms in 1 second -> Speed per ms
		var thresSpdMS = (abs(charTempHitKickbackSpd) * GAMESTEP)/1000;
		
		//Since there cannot be a fractional frame, we compensate by partially moving the player on the last frame
		//If somehow the time remaining become negative, we'll move the the player partially back (delta time would've moved us too much)
		x+=thresKickRemaining*thresSpdMS*sign(charTempHitKickbackSpd); //Then move partially with our set spd
		
		//Now convert the run or walk speed into pixels per ms.
		//There's 60 frames per second (We'll use the function for now. It could change)
		//Distance per second = speed * 60
		//Distance per ms = (Distance per second) / 1000
		//*
		xAxis = (moveRight) - (moveLeft);
		if xAxis != 0 {
			if moveRun {
				var thresSpdMS = (charRunSpeed * game_get_speed(gamespeed_fps))/1000;
			} else {
				var thresSpdMS = (charWalkSpeed * game_get_speed(gamespeed_fps))/1000;
			}
			
			//Now find the difference between the minimum frame time and remaining kickback
			//This is how long the player should've moved, if the kickback has ended
			var thresMoveRemaining = ((1/GAMESTEP)*1000)*delta - thresKickRemaining;
			
			show_debug_message(thresMoveRemaining);
			show_debug_message(thresSpdMS);
			show_debug_message("------");
			x+=thresMoveRemaining*thresSpdMS*xAxis; //Move in the same direction as the kickback (It's unlikely the player would be able to switch direction in that short amount of time)
		//}
		//*/
		
		//Debug. Measures distance
		x2 = x;
		y2 = y;
	}
	xSpeed = 0;
	charState = groundWalk;
}

charHitRecieveTimer -= delta * (1/GAMESTEP) * 1000;

horizontalCollision();
verticalCollision();