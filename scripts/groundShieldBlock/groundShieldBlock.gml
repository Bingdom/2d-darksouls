//We have finished the equiping action. Now it's time to actually block
if charShieldIsAction and get_timer_ms() - charShieldEquipTimer > charShieldEquip {
	if !charIsBlocking and !charShieldToUnblock {
		charShieldTimer    = get_timer_ms();
		charIsBlocking     = true;  //Is now blocking
		charShieldIsAction = false; //No longer between actions
	} else {
		//Unblock
		charIsBlocking     = false;
		charShieldIsAction = false; //No longer between actions
	}
}

//Override our charShieldTimer
if charIsBlocking and !(moveDown) {
	charShieldIsAction   = true; //Now inbetween actions
	charShieldEquipTimer = get_timer_ms(); //Start timer for holstering action
	charShieldToUnblock  = true; //Now it must unblock in the next action
	charIsBlocking		 = false;
}

//Not between an action and not blocking. Our state has ended
if !charShieldIsAction and !charIsBlocking {
	charShieldRecoTimer = get_timer_ms();
	charState           = groundWalk;
	
	charShieldToUnblock = false; //Stop overriding
	
	horizontalCollision();
	verticalCollision();
	
	exit;
}

//If we are a timer and we have held up our shield for too long
//This happens after the state change
if charShieldHasTimer and !charShieldIsAction and get_timer_ms() - charShieldTimer > charShieldMax  {
	charShieldIsAction   = true; //Now inbetween actions
	charShieldEquipTimer = get_timer_ms(); //Start timer for holstering action
	
	charShieldToUnblock  = true;
	charIsBlocking       = false;
}

horizontalCollision();
verticalCollision();