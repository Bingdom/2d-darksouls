//If the player has ordered to attack, we immediately skip attempting to move
if (moveAtkLT) and charStamina >= charStamAtkLT {
	charState       = groundAttack;
	charAttackType  = ATK_SWING_LIGHT;
	charAttackStage = ATK_STAGE_WINDUP;
	
	xSpeed=0;
	charAttackTimer = get_timer_ms();
	
	verticalCollision(); //It's expected we always do a collision check
	exit;
}
if (moveAtkHY) and charStamina >= charStamAtkHY {
	charState       = groundAttack;
	charAttackType  = ATK_SWING_HEAVY;
	charAttackStage = ATK_STAGE_WINDUP;
	
	xSpeed=0;
	charAttackTimer = get_timer_ms();
	
	verticalCollision();
	exit;
}
//Block only if a certain time has passed between the previous block
if (moveDown) and get_timer_ms() - charShieldRecoTimer > charShieldBetween  {
	charState            = groundShieldBlock;	
	charShieldEquipTimer = get_timer_ms(); //Start timer for holstering
	charShieldIsAction   = true; //It is between changing states
	xSpeed=0;
	
	verticalCollision();
	exit;
}

xAxis = (moveRight)-(moveLeft);
if xAxis != 0 {
	image_xscale = xAxis;
}

//Double press the same move again to dash
if charDashingTime==0 and charStamina > charStamDash { //If not already dashing
	if (moveRightPressed) {
		//If the previous move is the same move as the current and we pressed it again quickly,
		if thresDashmove == keyRight and get_timer_ms()-thresDashTime <= thresDash {
			//Dash
			charDashingTime = get_timer_ms();
			charDashDir = 1;
			xSpeed = charDashSpeed*charDashDir;
			
			//Dash takes away stamina
			charStaminaTake(charStamDash);
			
			//Debug
			x1 = x;
			y1 = y;
		} else {
			thresDashmove = keyRight; //Set as current move as this move
			thresDashTime = get_timer_ms(); //Set time as now
		}
	}
	if (moveLeftPressed) {
		//If the previous move is the same move as the current and we pressed it again quickly,
		if thresDashmove == keyLeft and get_timer_ms()-thresDashTime <= thresDash {
			//Dash
			charDashingTime = get_timer_ms();
			charDashDir = -1;
			xSpeed = charDashSpeed*charDashDir;
			
			//Dash takes away stamina
			charStaminaTake(charStamDash);
		
			//Debug
			x1 = x;
			y1 = y;
		} else {
			thresDashmove = keyLeft; //Set as current move as this move
			thresDashTime = get_timer_ms(); //Set time as now
		}
	}
}

//Press a move to run


//Note: movementDash is the dash script (although it's acting like a condition)
if !movementDash() {
	//if xAxis != 0 {
		if moveRun { //Is sprinting
			xSpeed = xAxis * charRunSpeed;
			charStaminaTake(charStamRun);
		} else { //Not sprinting
			xSpeed = xAxis * charWalkSpeed;
		}
		//xSpeed =(xAxis)*(charWalkSpeed+moveRun*(charRunSpeed-charWalkSpeed));
	//} else {
	//	xSpeed = 0;
	//}
}
//----- Collisions ------
horizontalCollision();

if verticalCollision() {
	if (moveUp) and get_timer_ms() - thresJumpAgainTime > thresJumpAgain {
		ySpeed=-charJumpStr;
		charState = airJump;
		thresJumpAgainTime = get_timer_ms();
	}
} else {
	//The player may be falling.
	charState = airJump;
}