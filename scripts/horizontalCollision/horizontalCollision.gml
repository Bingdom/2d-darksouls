// Horizontal Collision -
if tilemap_get_at_pixel(collisionMap, x+xSpeed, y) {
	while !tilemap_get_at_pixel(collisionMap, x, y) {
		x+=sign(xSpeed);	
	}
	xSpeed=0;
	return true;
}
return false;