//Note: This similar concept is applied to groundKickedback

//Returns true if dashing. Returns false if not dashing.
//We add by game speed because we want to predict when we'll stop dashing, so the last frame can be a partial dash.
//It's impossible to exactly predict how long it would take to complete the next frame
//We can only roughly predict how long it will take using our current performance (delta time)
//LHS would be larger if it is less than the minimum time remaining to complete a frame
if ((get_timer_ms() - charDashingTime + ((1/GAMESTEP)*1000)*delta) < charDashingDur) {
	//We have plenty of time. Dash normally.
	xSpeed = charDashDir * charDashSpeed;
	
	return true;
} else {
	/**
	 * This is for the last dash frame
	 * This makes sure we are exactly in the right spot after the dash (The time is not bound to frames, unlike controls)
	 * This is important for not only just precise fighting games, but for multiplayer too. Clients don't have exactly the same "step cycle".
	 *
	 * The underlying principle is this;
	 *   distance = velcity * time
	 *
	 * We have assigned our speed and time. We just make sure the frames don't break that
	 */
	if (charDashingTime != 0) {
		//Find the difference from the last frame, since we are delta timing and want unlocked fps (rather than using fixed numbers).
		//We add charDashingDur because it's supposed to have that difference (look at the first if condition)
		var thresDashRemaining = charDashingTime - get_timer_ms() + charDashingDur;
		//thresDashRemaining is the remaining ms. It would always be less than 1/TARGET_FPS.
		
		show_debug_message(thresDashRemaining);
		
		//Find our speed in pixels per ms
		//Speed per frame * Speed per second / ms in 1 second -> Speed per ms
		var thresSpdMS = (charDashSpeed * GAMESTEP)/1000;
		
		//Since there cannot be a fractional frame, we compensate by partially moving the player on the last frame
		//If somehow the time remaining become negative, we'll move the the player partially back (delta time would've moved us too much)
		x+=thresDashRemaining*thresSpdMS*charDashDir; //Then move partially with our set spd
		
		//Now convert the run or walk speed into pixels per ms.
		//There's 60 frames per second (We'll use the function for now. It could change)
		//Distance per second = speed * 60
		//Distance per ms = (Distance per second) / 1000
		//*
		xAxis = (moveRight) - (moveLeft);
		if xAxis != 0 {
			if moveRun {
				var thresSpdMS = (charRunSpeed * game_get_speed(gamespeed_fps))/1000;
			} else {
				var thresSpdMS = (charWalkSpeed * game_get_speed(gamespeed_fps))/1000;
			}
			
			//Now find the difference between the minimum frame time and remaining dash
			//This is how long the player should've moved, if the dash has ended
			var thresMoveRemaining = ((1/GAMESTEP)*1000)*delta - thresDashRemaining;
			
			show_debug_message(thresMoveRemaining);
			show_debug_message(thresSpdMS);
			show_debug_message("------");
			x+=thresMoveRemaining*thresSpdMS*xAxis; //Move in the same direction as the dash (It's unlikely the player would be able to switch direction in that short amount of time)
		}
		//*/
		
		//Reset back to 0, so this doesn't get executed again
		charDashingTime=0;
		
		//Debug. Measures distance
		x2 = x;
		y2 = y;
		
		xSpeed=0;
		return true; //We have moved the player enough this step. We would give the player control again the next step
	}
	return false;
}