//- Vertical Collision --
if tilemap_get_at_pixel(collisionMap, x, bbox_bottom+ySpeed) {
	while !tilemap_get_at_pixel(collisionMap, x, bbox_bottom) {
		y+=sign(ySpeed);	
	}
	ySpeed=0;
	return true;
}
return false;