{
    "id": "8795c4df-bad8-42f5-8d25-a1ea167952bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a96261d-6211-4bad-b531-f099e74e6886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8795c4df-bad8-42f5-8d25-a1ea167952bf",
            "compositeImage": {
                "id": "3a0078ca-8064-4693-acbd-57d5a921cf65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a96261d-6211-4bad-b531-f099e74e6886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffabaf06-2d3b-4482-9156-7290828b29ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a96261d-6211-4bad-b531-f099e74e6886",
                    "LayerId": "098492f2-bdf4-4c35-b88f-3b9a29c4c757"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "098492f2-bdf4-4c35-b88f-3b9a29c4c757",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8795c4df-bad8-42f5-8d25-a1ea167952bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}