{
    "id": "288b78a3-1961-4808-8740-2ddb5e4d69f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharacter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c80e2f0-a734-480e-a8a9-ac0f8c1414ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "288b78a3-1961-4808-8740-2ddb5e4d69f2",
            "compositeImage": {
                "id": "3d2f9a89-8c96-4c7c-a8d0-abb4dbc42402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c80e2f0-a734-480e-a8a9-ac0f8c1414ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5fc3e2-8d68-4af6-8171-41d6138b23b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c80e2f0-a734-480e-a8a9-ac0f8c1414ce",
                    "LayerId": "92631985-e9ac-401b-a248-cec26f515620"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "92631985-e9ac-401b-a248-cec26f515620",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "288b78a3-1961-4808-8740-2ddb5e4d69f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}