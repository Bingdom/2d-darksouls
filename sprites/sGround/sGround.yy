{
    "id": "6abe52e6-598d-43ce-9315-a2a54be9e755",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e60af8f2-5d7c-4519-8bb5-1a997cc10b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abe52e6-598d-43ce-9315-a2a54be9e755",
            "compositeImage": {
                "id": "5488ed3b-35e7-4b12-9bd3-5c0a723df301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60af8f2-5d7c-4519-8bb5-1a997cc10b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba5f4037-cb29-4990-9da5-7f0218972ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60af8f2-5d7c-4519-8bb5-1a997cc10b05",
                    "LayerId": "270ee5e7-0103-492d-b019-df45da4a9793"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "270ee5e7-0103-492d-b019-df45da4a9793",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6abe52e6-598d-43ce-9315-a2a54be9e755",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}