{
    "id": "28a6cddf-e11c-47a9-a2e7-208094481127",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c8e62cf-6741-4308-9584-f4adeea2af00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a6cddf-e11c-47a9-a2e7-208094481127",
            "compositeImage": {
                "id": "321cc24b-11d8-47ee-ae4f-409ce5e52ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c8e62cf-6741-4308-9584-f4adeea2af00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07ff409-55dd-44c2-ab78-c914b9f174d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c8e62cf-6741-4308-9584-f4adeea2af00",
                    "LayerId": "e237ea98-d93d-459d-ac75-a2e756d72747"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e237ea98-d93d-459d-ac75-a2e756d72747",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28a6cddf-e11c-47a9-a2e7-208094481127",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 8
}